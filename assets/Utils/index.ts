import { Node, UITransform, Layers } from 'cc'

// 创建UI组件并设置基础属性
export const createUINode = (name: string = '') => {
    const node = new Node(name)
    // 添加UI组件
    const transform = node.addComponent(UITransform)
    //设置原点
    transform.setAnchorPoint(0,1)
    //设置layer
    node.layer = 1 << Layers.nameToLayer("UI_2D")
    return node
}

// 生成区间随机数
export const randomByRange = (start: number, end: number): number =>{
    return start + Math.floor((end - start) * Math.random())
}
