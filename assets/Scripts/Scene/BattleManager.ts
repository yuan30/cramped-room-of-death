import { _decorator, Component, Node } from 'cc'
import levels, { ILevel } from '../../Levels'
import DataManager from '../../Runtime/DataManager'
import { createUINode } from '../../Utils'
import { TILE_HEIGHT, TILE_WIDTH } from '../Tile/TileManager'
import { TileMapManager } from '../Tile/TileMapManager'
const { ccclass, property } = _decorator

/**
 * Predefined variables
 * Name = BattleManager
 * DateTime = Tue Jul 05 2022 16:40:00 GMT+0800 (中国标准时间)
 * Author = Zero_Y
 * FileBasename = BattleManager.ts
 * FileBasenameNoExtension = BattleManager
 * URL = db://assets/Scripts/Scene/BattleManager.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */

@ccclass('BattleManager')
export class BattleManager extends Component {
  // [1]
  // dummy = '';

  // [2]
  // @property
  // serializableDummy = 0;

  level: ILevel
  stage: Node

  start() {
    this.generateStage()
    this.initLevel()
  }

  initLevel(){
    const level = levels[`level${1}`]
    if(level){
      this.level = level
      // 将数据存入数据中心
      DataManager.Instance.mapInfo = this.level.mapInfo
      DataManager.Instance.mapRowCount = this.level.mapInfo.length || 0
      DataManager.Instance.mapColumnCount = this.level.mapInfo[0].length || 0
      this.generateTileMap()
    }
  }

  // 创建stage
  generateStage(){
    this.stage = createUINode()
    this.stage.setParent(this.node)
  }
  //创建地图
  generateTileMap() {
    // 生成瓦片地图节点
    const tileMap = new Node()
    // 设置父级
    tileMap.setParent(this.stage)
    // 给瓦片添加脚本
    const tileMapManager = tileMap.addComponent(TileMapManager)
    // 执行脚本初始化方法
    tileMapManager.init()
    this.adaptPos()
  }
  // 对地图进行居中适配
  adaptPos(){
    // 向左上偏移自身宽度的一半
    const disX = TILE_WIDTH * DataManager.Instance.mapRowCount / 2
    const disY = TILE_HEIGHT * DataManager.Instance.mapColumnCount / 2
    this.stage.setPosition(-disX, disY)
  }
}
