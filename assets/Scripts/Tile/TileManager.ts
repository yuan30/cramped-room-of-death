import { _decorator, Component, SpriteFrame, Sprite, UITransform } from 'cc'
const { ccclass, property } = _decorator

/**
 * Predefined variables
 * Name = TileManager
 * DateTime = Tue Jul 05 2022 16:53:17 GMT+0800 (中国标准时间)
 * Author = Zero_Y
 * FileBasename = TileManager.ts
 * FileBasenameNoExtension = TileManager
 * URL = db://assets/Scripts/Tile/TileManager.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
export const TILE_WIDTH = 55
export const TILE_HEIGHT = 55

@ccclass('TileManager')
export class TileManager extends Component {
  start() {
    // [3]
  }
  async init(spriteFrame: SpriteFrame, i:number, j:number) {
   const sprite = this.addComponent(Sprite)
   sprite.spriteFrame = spriteFrame
   const transform = this.getComponent(UITransform)
   transform.setContentSize(TILE_WIDTH, TILE_HEIGHT)
   this.node.setPosition(i*TILE_WIDTH, -j*TILE_HEIGHT)
  }
}
