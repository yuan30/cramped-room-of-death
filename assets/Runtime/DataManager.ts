import Singleton from "../Base/Singleton"
import { ITile } from "../Levels"
/**
 * 数据单例
 */
export default class DataManager extends Singleton{
    static get Instance(){
        return super.GetInstance<DataManager>()
    }
    mapInfo: Array<Array<ITile>>
    mapRowCount: number //行数
    mapColumnCount: number //列数
}
export const DataManagerInstance = new DataManager()
