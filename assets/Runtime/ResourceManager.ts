/**
 * 资源加载
 */

import { resources, SpriteFrame } from "cc";
import Singleton from "../Base/Singleton";

export default class ResourceManager extends Singleton{

    static get Instance(){
        return super.GetInstance<ResourceManager>()
    }

    loadDir(path: string, type: typeof SpriteFrame = SpriteFrame ){
        return new Promise<SpriteFrame[]>((resolve, reject) => {
          resources.loadDir(path, type, function (err, assets) {
            if(err){
              reject()
            } else{
              resolve(assets)
            }
          });
        })
      }

}
